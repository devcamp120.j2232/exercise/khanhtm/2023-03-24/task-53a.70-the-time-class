public class Time {
 /**
  * THuộc tính
  */
 private int hour; 
 private int minute; 
 private int second; 
 /**
  * Phương thức
  */
  public Time( int hour, int minute, int second){
   if (hour <= 23 && hour >= 0) {
    this.hour = hour;
   }
   if (minute <= 59 && minute >= 0) {
    this.minute = minute;
   }
   if (second <= 59 && second >= 0) {
    this.second = second;
   }
  }
  /**
   * Getter method
   * @return
   */
  public int getHour() {
   return hour;
  }

  public int getMinute() {
   return minute;
  }

  public int getSecond() {
   return second;
  }
  /**
   * Setter method
   * @param
   */
  public void setHour(int hour) {
   if (hour <= 23 && hour >= 0) {
    this.hour = hour;
   }
  }

  public void setMinute(int minute) {
   if (minute <= 59 && minute >= 0) {
    this.minute = minute;
   }
  }

  public void setSecond(int second) {
   if (second <= 59 && second >= 0) {
    this.second = second;
   }
  }
  public void setTime( int hour, int minute, int second){
   if (hour <= 23 && hour >= 0) {
    this.hour = hour;
   }
   if (minute <= 59 && minute >= 0) {
    this.minute = minute;
   }
   if (second <= 59 && second >= 0) {
    this.second = second;
   }
  }

  public String toString() {
   return hour+ ":" + minute+ ":" + second;
  }

  public void nextSecond(){
   this.second = this.second + 1;
  }
  public void previousSecond(){
   this.second = this.second - 1;
  }
}
