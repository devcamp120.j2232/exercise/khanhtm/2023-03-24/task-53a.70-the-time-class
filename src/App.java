public class App {
    public static void main(String[] args) throws Exception {
        // Create a new Object
        Time time1 = new Time(13,50,50);
        Time time2 = new Time(22,39,29);
        //print to console log
        System.out.println(time1.toString());
        System.out.println(time2.toString());
        //Tăng giảm 1 s
        time1.nextSecond();
        time2.previousSecond();
         //print to console log
         System.out.println(time1.toString());
         System.out.println(time2.toString());

    }
}
